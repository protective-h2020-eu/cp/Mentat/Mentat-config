#-------------------------------------------------------------------------------
#        CONFIGURATION FILE FOR DEMONSTRATION SETUP OF MENTAT SYSTEM
#
# Usage:
#   Make sure none of the Mentat modules is running on the system and then
#   bootstrap the demonstration setup by executing following command:
#
#       mentat-controller.py --config-file=demo.mentat-controller.py.conf
#
# Warning:
#   This file is deliberately not flagged as config file in Debian package
#   metadata and any changes will be overwritten with next update.
#
# Copyright (C) since 2011 CESNET, z.s.p.o
# Use of this source is governed by the MIT license, see LICENSE file.
#-------------------------------------------------------------------------------

{
    #---------------------------------------------------------------------------
    # Custom daemon configurations
    #---------------------------------------------------------------------------

    # List of the inspection rules. The rules will be processed in
    # following order.
    "inspection_rules":  [

        {
            # OPTIONAL name for the rule, should be short and will be
            # used as title in log files and email reports. When not set,
            # this defaults to "rule" (see further below).
            "name": "Example commented rule",

            # OPTIONAL description for the rule. This may be much longer
            # text and it should describe a purpose of the rule. Description
            # will appear in email reports, so it may also contain hints
            # for recipients as how to handle the report.
            "description": "This rule should really not match any of your alerts",

            # MANDATORY rule expression. Sadly, currently there is no
            # documentation of the filtering language, please see the
            # files in mentat.filtering package. The language is fairly
            # simple and all common logical, comparison and math operators
            # are available. NOTE: This is only example rule, "0 AND" will
            # never be True.
            "rule": "0 AND Category EQ \"Recon.Scanning\"",

            # Fallback rule, mathing all alerts may be created with following trick:
            #"rule": "1",

            # OPTIONAL final flag, defaults to False. When set to True,
            # mathing rule will make the inspection stop after this rule,
            # otherwise next inspection rule will be evaluated.
            "final": false,

            # OPTIONAL enabled flag, defaults to True. When set to False,
            # the rule will be disabled from processing.
            "enabled": true,

            # MANDATORY actions list, may be empty. This should containing
            # all actions to be processed upon matching the rule.
            #
            # WARNING: ALL ARGUMENTS FOR ANY PARTICULAR ACTION ARE MANDATORY.
            "actions": [

                # Tag given message at given path with given static value.
                {"action": "tag", "args": {"path": "TestA.ValueA1",  "value": "Recon alert", "overwrite": true, "unique": false}},

                # Set given message at given path with result of given expression.
                {"action": "set", "args": {"path": "TestA.ValueA2", "expression": "CreateTime + 3600", "overwrite": true, "unique": false}},

                # Send given message as email report to given email.
                {"action": "report",   "args": {"to": "recipient@organization.com", "from": "sender@organization.com", "subject": "Inspection alert", "report_type": "inspection_alert"}},

                # Duplicate (copy) given message into another queue.
                {"action": "duplicate", "args": {"queue_tgt": "/var/tmp"}},

                # Dispatch (move) given message into another queue.
                # IMPORTANT: This rule will stop further processing of
                # current message.
                {"action": "dispatch", "args": {"queue_tgt": "/var/tmp"}},

                # Stop processing of given message and drop it from queue.
                # IMPORTANT: This rule will stop further processing of
                # current message.
                {"action": "drop"},

                # Logging action, usefull mostly for debugging and testing purposes.
                {"action": "log", "args": {"label": "User description"}}
            ]
        },

        # Example of minimal configuration:
        {
            "rule": "0 AND Category EQ \"Attempt.Exploit\"",
            "actions": [
                {"action": "tag", "args": {"path": "TestA.ValueA1", "value": "Exploit alert"}}
            ]
        },

        # Fallback, or catch-all rule, that is matching all alerts, may
        # be created with following trick:
        {
            "name": "Fallback rule",
            "description": "Fallback rule matching all alerts, currently disabled by configuration",
            "rule": "1",
            "enabled": false,
            "actions": [
                {"action": "tag", "args": {"path": "Test.Value", "value": "Fallback action"}}
            ]
        }

    ],

    # List of fallback actions, that will be performed with given message
    # in case it does NOT match any of the inspection rules.
    "fallback_actions": [

        # Perhaps make copy of unknown alert to separate folder for further inspection?
        #{"action": "duplicate", "args": {"queue_tgt": "/var/mentat/spool/..."}}

        # Perhaps just log the thing into the log file
        {"action": "log", "args": {"label": "User description for fallback action"}}

    ],

    #---------------------------------------------------------------------------
    # Common piper daemon configurations
    #---------------------------------------------------------------------------

    # Name of the input queue directory.
    #   default: "/var/mentat/spool/mentat-inspector.py"
    #   type:    string
    #
    #"queue_in_dir": "/var/mentat/spool/mentat-inspector.py",

    # Name of the output queue directory.
    #   default: null
    #   type:    string
    #
    "queue_out_dir": "/var/mentat/spool/mentat-enricher.py",

    # Limit on the number of the files for the output queue directory.
    #   default: 10000
    #   type:    integer
    #
    #"queue_out_limit": 10000,

    # Waiting time when the output queue limit is reached in seconds.
    #   default: 30
    #   type:    integer
    #
    #"queue_out_wait": 30,

    #---------------------------------------------------------------------------
    # Common daemon configurations
    #---------------------------------------------------------------------------

    # Do not daemonize and stay in foreground (flag).
    #   default: false
    #   type:    boolean
    #
    #"no_daemon": false,

    # Name of the chroot directory.
    #   default: null
    #   type:    string
    #
    #"chroot-dir": null,

    # Name of the process work directory.
    #   default: "/"
    #   type:    string
    #
    #"work_dir": "/",

    # Name of the pid file.
    #   default: "/var/mentat/run/mentat-inspector.py.pid"
    #   type:    string
    #
    #"pid_file": "/var/mentat/run/mentat-inspector.py.pid",

    # Name of the state file.
    #   default: "/var/mentat/run/mentat-inspector.py.state"
    #   type:    string
    #
    #"state_file": "/var/mentat/run/mentat-inspector.py.state",

    # Default file umask.
    #   default: "0o002"
    #   type:    string
    #
    #"umask": "0o002",

    # Processing statistics display interval in seconds.
    #   default: 300
    #   type:    integer
    #
    "stats_interval": 30,

    # Run in paralel mode (flag).
    #   default: false
    #   type:    boolean
    #
    #"paralel": false,

    #---------------------------------------------------------------------------
    # Common application configurations
    #---------------------------------------------------------------------------

    # Run in quiet mode (flag).
    #   default: false
    #   type:    boolean
    #
    #"quiet": false,

    # Application output verbosity.
    #   default: 0
    #   type:    int
    #
    #"verbosity": 0,

    # Name of the log file.
    #   default: "/var/mentat/log/mentat-inspector.py.log"
    #   type:    string
    #
    #"log_file": "/var/mentat/log/mentat-inspector.py.log",

    # Logging level ['debug', 'info', 'warning', 'error', 'critical'].
    #   default: "info"
    #   type:    string
    #
    "log_level": "debug",

    # Name of the runlog directory.
    #   default: "/var/mentat/run/mentat-inspector.py"
    #   type:    string
    #
    #"runlog_dir": "/var/mentat/run/mentat-inspector.py",

    # Dump runlog to stdout when done processing (flag).
    #   default: false
    #   type:    boolean
    #
    #"runlog_dump": false,

    # Write runlog to logging service when done processing (flag)
    #   default: false
    #   type:    boolean
    #
    #"runlog_log": false,

    # Name of the persistent state file.
    #   default: "/var/mentat/run/mentat-inspector.py.pstate"
    #   type:    string
    #
    #"pstate_file": "/var/mentat/run/mentat-inspector.py.pstate",

    # Dump persistent state to stdout when done processing (flag).
    #   default: false
    #   type:    boolean
    #
    #"pstate_dump": false,

    # Write persistent state to logging service when done processing (flag).
    #   default: false
    #   type:    boolean
    #
    #"pstate_log": false,

    # Name of the quick action to be performed.
    #   default: null
    #   type: string
    #
    #"action": null,

    # Name/uid of the user.
    #   default: null
    #   type:    string or integer
    #
    "user": "mentat",

    # Name/gid of the group.
    #   default: null
    #   type:    string or integer
    #
    "group": "mentat"
}
