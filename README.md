PROTECTIVE  Mentat Configuration
========== 

This repository holds all that is neccessary to start a particular `Mentat` configuration. The particular configuration can be selected by checking out the appropriate branch or tag. It is recommended that, at a minimum, all production configurations are stored in an appropriate branch or tag in this repository. 

Prerequisites are that the machine (or a VM) has `git` and `docker` installed.

Mentat is made up of two products, `Mentat` and `Mentat-perl`, which are distributed as docker images, each stored in a different docker registry location on GitLab. A MongoDB image, located in DockerHub, is also installed on the same machine.

To install the products first `git clone` this repository then `cd` into the working directory, which defaults to `Mentat-config`, and checkout the appropriate branch/tag, e.g. `master`.

To modify the configuration (only if neccessary as default configuration will work for demonstration) edit files in the `data/conf` folder. These will be picked up by the docker continers when they are started. Consider saving any changes to the configuration to a new branch or tag.

If it is neccessary to access `Mentat` from a remote machine then edit the `docker-compose.yml` file and change the line

```
      - "127.0.0.1:9080:80"
```
to
```
      - "9080:80"
```
Note that this is not secure as https and passwords are not implemented yet.

If you are not already logged in to GitLab's docker registry (usually done once, use your GitLab username and password):

```
docker login registry.gitlab.com
```

To download the 3 images (the first time or when images in the GitLab registry or DockerHub have changed):

```
docker-compose pull
```

To start the containers:

```
docker-compose up -d
```

The `Mentat` web site can be accessed at `http://localhost:9080` or a particular IP or hostname if the `docker-compose.yml` file has been edited as described earlier.

In order to demonstrate `Mentat` you can generate synthetic alerts by doing the following:

```
docker exec -it mentat_server_1 bash
```
Then at the new `bash` prompt (inside the container):

```
mentat-ideagen.py --count 1000
```
Statistics take a few minutes to be generated, provided alerts have been generated first.

To stop the containers:

```
docker-compose down
```

In future robust security will be applied to Mentat with https, SSL, passwords, etc.  